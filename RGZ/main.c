#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <memory.h>

#define INFINT 2147483646
#define ROWMJR(R, C, NR, NC) (R*NC+C)
#define a(R, C) a[ROWMJR(R,C,ln,n)]

#define MAIN_PROCESS 0
#define SEND_NUM_TAG 0
#define SEND_DISPLS_TAG 1
#define SEND_ELEMNTS_TAG 2
#define SEND_WEIGHT_TAG 3
#define SEND_COUNTS_TAG 4
#define SEND_RESULT_TAG 5

// функция расчета смещений и определения кол-ва элементов для каждого процесса
static void calculateDispls(int ** displs, int ** localNumOfElements, int numberOfProcessors, int size){
	int local, reminder;
	if (size < numberOfProcessors){//Если число процессоров больше, чем строк (столбцов), дополнительные процессоры бесполезны.
		local = 1;
		*localNumOfElements = malloc(numberOfProcessors * sizeof(**localNumOfElements));
		int i = 0;
		for (; i < size; i++){
			*(*localNumOfElements + i) = local;
		}
		for (; i < numberOfProcessors;i++){
			*(*localNumOfElements + i) = 0;
		}
	} else {//Если строк больше числа доступных процессов, разделить их на практически равные части.
		local = size / numberOfProcessors;
		reminder = size % numberOfProcessors;
		
		// расчитать числа для отправки каждому прооцессу
		*localNumOfElements = malloc(numberOfProcessors * sizeof(**localNumOfElements));
		for (int i = 0; i < numberOfProcessors; i++) {
			*(*localNumOfElements + i) = local;
		}
		*(*localNumOfElements + numberOfProcessors - 1) += reminder;
	}
	// рассчитать смещения точек для отправляемого буфера
	*displs = malloc(numberOfProcessors * sizeof(**displs));
	for (int i = 0; i < numberOfProcessors; i++) {
		*(*displs + i) = 0;
		for (int j = 0; j < i; j++) {
			*(*displs + i) += *(*localNumOfElements + j);
		}
	}
}


static void load(
		char const *const filename,
		int *const np,
		int **const ap, int numberOfProcessors, int ** displs, int ** localNumOfElements, int rank
) {
	int n;
	int *a = NULL;
	if (rank == MAIN_PROCESS) {//главный процесс читает файл и отправляет другим по частям
		int i, j, k;
		FILE *fp = NULL;
		
		fp = fopen(filename, "r");
		
		// считать число вершин в графе
		fscanf(fp, "%d", &n);
		
		//Посчитать, сколько строк будет содержать каждый процесс, и их смещения(displs).
		calculateDispls(displs, localNumOfElements, numberOfProcessors, n);
		
				
		// чтение весов (ВСЕХ)
		a = malloc(n * n * sizeof(*a));
		/*for (j = 0; j < n * n; j++) {
			fscanf(fp, "%d", &a[j]);
		}*/
		int prob;
		for(i = 0; i < n; i++){
			for(int j = i + 1; j < n; j++){
				prob = rand() % 5;
				if(prob < 2)
					a[i*n + j] = rand() % 19 + 1;
				else
					a[i*n + j] = 0;
				a[j*n + i] = a[i*n + j];
			}
			a[i*n + i] = 0;
		}
		*ap = a;
		
		//Read & send. Каждый процесс получит localNumOfElements строк
		for (i = 1; i < numberOfProcessors; i++) {
			// отправить число вершин
			MPI_Send(&n, 1, MPI_INTEGER, i, SEND_NUM_TAG, MPI_COMM_WORLD);
			// отправить смещения
			MPI_Send(*displs, numberOfProcessors, MPI_INTEGER, i, SEND_DISPLS_TAG, MPI_COMM_WORLD);
			// отправить числа строк для каждого процесса
			MPI_Send(*localNumOfElements, numberOfProcessors, MPI_INTEGER, i, SEND_ELEMNTS_TAG, MPI_COMM_WORLD);
			
			j = n * n;
			// отправить размер массива весов
			MPI_Send(&j, 1, MPI_INTEGER, i, SEND_COUNTS_TAG, MPI_COMM_WORLD);
			// отправить массив весов
			MPI_Send(a, j, MPI_INT, i, SEND_WEIGHT_TAG, MPI_COMM_WORLD);
			// Очистить память после отправки
		}
		
		fclose(fp);
    } else {// Все узлы, кроме первого, получать стоки матрицы (графа)
		int count;
		// получить число вершин
		MPI_Recv(&n, 1, MPI_INTEGER, MAIN_PROCESS, SEND_NUM_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		*displs = malloc(numberOfProcessors * sizeof(**displs));
		// получить смещение
		MPI_Recv(*displs, numberOfProcessors, MPI_INTEGER, MAIN_PROCESS, SEND_DISPLS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		*localNumOfElements = malloc(numberOfProcessors * sizeof(**localNumOfElements));
		MPI_Recv(*localNumOfElements, numberOfProcessors, MPI_INTEGER, MAIN_PROCESS, SEND_ELEMNTS_TAG, MPI_COMM_WORLD,
				MPI_STATUS_IGNORE);
		// получить размер массива весов
		MPI_Recv(&count, 1, MPI_INTEGER, MAIN_PROCESS, SEND_COUNTS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		a = malloc(count * sizeof(*a));
		// получить массив весов
		MPI_Recv(a, count, MPI_INT, MAIN_PROCESS, SEND_WEIGHT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		*ap = a;
	}
	*np = n;
	MPI_Barrier(MPI_COMM_WORLD);
}

static void dijkstra(
		int const n,
		int const *const a,
		int **const result, int rank, int * displs, int * localNumOfElements, int numberOfProcessors
) {
	const int source = 0;
	int i, j, k, sourceNode = 0;
	
	int loc_min[2], glbl_min[2];
	
	char *visited = NULL;
	int *resultVector = NULL;
	int * localResult = NULL;
	
	int start_pos = displs[rank];
	int end_pos = start_pos + localNumOfElements[rank];
	// DEBUGING
	//printf("rank = %d, displs[%d] = %d, localNumOfElements[%d] = %d\n", rank, rank, displs[rank], rank, localNumOfElements[rank]);
	
	//массив set для записей уже посещенных вершин(с исправленными минимальными дистанциями).
	//0 - непосещенная вершина.
	visited = calloc(n, sizeof(int));
	
	// массив для хранения расстояний от заданной вершины до других
	resultVector = malloc(n * sizeof(*resultVector));
	localResult = malloc(n * sizeof(*resultVector));
	
	
	//localResult[0] = INFINT;
	for(int i = 1; i < n; i++){
		resultVector[i] = INFINT;
		localResult[i] = INFINT;
	}
	
	resultVector[source] = localResult[source] = 0;
	 
	
	do{
	
		loc_min[0] = INFINT;
		loc_min[1] = INFINT;
			
		
		// Найти локальную вершину с минимальным весом и обновить минимальное расстояние 
		// до каждой вершины, с которой она связана.
		for (j = start_pos; j < end_pos; j++) {
			if (!visited[j] && resultVector[j] < loc_min[0]) {
				loc_min[0] = resultVector[j];
				loc_min[1] = j;
			}
			localResult[j] = resultVector[j];
		}
		MPI_Allreduce(loc_min, glbl_min, 1, MPI_2INT, MPI_MINLOC, MPI_COMM_WORLD);
		printf("Выбрана вершина %d. Минимальный вес: %d\n", glbl_min[1], glbl_min[0]);
		
		
		// каждый процесс сохраняет в свою позицию localResult 
		// значение суммы минимального веса и элемента из массива a.
		if(glbl_min[1] != INFINT){
			for (j = 0; j < localNumOfElements[rank]; j++){
				if ( a(glbl_min[1], displs[rank] + j) > 0 && a(glbl_min[1], displs[rank] + j) + glbl_min[0] < localResult[j + displs[rank]] ){
					localResult[j + displs[rank]] = a(glbl_min[1], displs[rank] + j) + glbl_min[0];
				}
			}
				
			// сохранение результата в адресном пространстве всех процессов 
			// (localResult в resultVector, при этом в resultVector будут 
			// сохранены минимальные значения из localResult всех процессов)
			MPI_Allreduce(localResult, resultVector, n, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
			//MPI_Barrier(MPI_COMM_WORLD);
			visited[glbl_min[1]] = 1;
		}
	
	} while(glbl_min[1] < INFINT);
	
	free(visited);
	*result = resultVector;
}

static void print_time(double const seconds) {
    printf("Operation Time: %0.04fs\n", seconds);
}

// Записать в файл длины путей от заданной вершины ко всем остальным
static void print_numbers(
		char const *const filename,
		int const n,
		int const *const numbers) {
	int i;
	FILE *fout;
	
	if (NULL == (fout = fopen(filename, "w"))) {
		fprintf(stderr, "error opening '%s'\n", filename);
		abort();
	}
	
	for (i = 0; i < n; i++) {
		fprintf(fout, "%d\n", numbers[i]);
	}
	fclose(fout);
}

int main(int argc, char **argv) {
	int n, numberOfProcessors, rank;
	double t1, t2;
	int *a = NULL, *result = NULL;
	int * displs = NULL, *localNumOfElements = NULL;
	
	if (argc < 3) {
		fprintf(stderr, "Usage: %s <graph> <output_file>.\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcessors);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	load(argv[1], &n, &a, numberOfProcessors, &displs, &localNumOfElements, rank);
	
	MPI_Barrier(MPI_COMM_WORLD);
	t1 = MPI_Wtime();
	
	dijkstra(n, a, &result, rank, displs, localNumOfElements, numberOfProcessors);
	t2 = MPI_Wtime();
	
	if (rank == MAIN_PROCESS) {
		print_time(t2 - t1);
		print_numbers(argv[2], n, result);
	}
	free(a);
	free(result);
	MPI_Finalize();
	return EXIT_SUCCESS;
}

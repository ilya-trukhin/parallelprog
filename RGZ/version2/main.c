#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <memory.h>

#define INFINT 2147483646
#define ROWMJR(R, C, NR, NC) (R*NC+C)
#define a(R, C) a[ROWMJR(R,C,ln,n)]

#define MAIN_PROCESS 0
#define SEND_NUM_TAG 0
#define SEND_DISPLS_TAG 1
#define SEND_ELEMNTS_TAG 2
#define SEND_WEIGHT_TAG 3
#define SEND_VERTECES_TAG 4
#define SEND_COUNTS_TAG 5
#define SEND_RESULT_TAG 6

// максимальное число смежных вершин
#define MAX_NEIGHBORING_VERTICES 2
// максимальное расстояние между двумя вершинами
#define MAX_WEIGHT 10

typedef struct vertex{
	int *verteces;		// указатель на массив смежных вершин
	int *weights;		// указатель на массив весов
	int count;			// общее число смежных верших
} vertex;

// функция расчета смещений и определения кол-ва элементов для каждого процесса
static void calculateDispls(int ** displs, int ** localNumOfElements, int numberOfProcessors, int size){
	int local, reminder;
	if (size < numberOfProcessors){//Если число процессоров больше, чем строк (столбцов), дополнительные процессоры бесполезны.
		local = 1;
		*localNumOfElements = malloc(numberOfProcessors * sizeof(**localNumOfElements));
		int i = 0;
		for (; i < size; i++){
			*(*localNumOfElements + i) = local;
		}
		for (; i < numberOfProcessors;i++){
			*(*localNumOfElements + i) = 0;
		}
	} else {//Если строк больше числа доступных процессов, разделить их на практически равные части.
		local = size / numberOfProcessors;
		reminder = size % numberOfProcessors;
		
		// расчитать числа для отправки каждому прооцессу
		*localNumOfElements = malloc(numberOfProcessors * sizeof(**localNumOfElements));
		for (int i = 0; i < numberOfProcessors; i++) {
			*(*localNumOfElements + i) = local;
		}
		*(*localNumOfElements + numberOfProcessors - 1) += reminder;
	}
	// рассчитать смещения точек для отправляемого буфера
	*displs = malloc(numberOfProcessors * sizeof(**displs));
	for (int i = 0; i < numberOfProcessors; i++) {
		*(*displs + i) = 0;
		for (int j = 0; j < i; j++) {
			*(*displs + i) += *(*localNumOfElements + j);
		}
	}
}

int find(int* array, int size, int elem){
	for(int i = 0; i < size; i++){
		if(array[i] == elem)
			return 1;
	}
	return 0;
}

static void load(
		char const *const filename,
		int *const np,
		vertex **const ap, int numberOfProcessors, int ** displs, int ** localNumOfElements, int rank
) {
	int n;
	vertex *a = NULL;
	
	
	if (rank == MAIN_PROCESS) {//главный процесс читает файл и отправляет другим по частям
		int i, j, k;
		FILE *fp = NULL;
		
		fp = fopen(filename, "r");
		
		// считать число вершин в графе
		fscanf(fp, "%d", &n);
		
		//Посчитать, сколько строк будет содержать каждый процесс, и их смещения(displs).
		calculateDispls(displs, localNumOfElements, numberOfProcessors, n);
				
		// чтение весов (ВСЕХ)
		//a = malloc(n * n * sizeof(*a));
		a = malloc(n * sizeof(vertex));
		
		for(int i = 0; i < n; i++){
			int count = MAX_NEIGHBORING_VERTICES;
			a[i].count = count;
			a[i].verteces =  malloc(count * sizeof(int));
			a[i].weights = malloc(count * sizeof(int));
		}
		
		int curcount = 0;
		// инициализация первого блока
		for(int i = 0; i < n / 4; i++){
			int weight = (rand () % (MAX_WEIGHT - 1)) + 1;
			a[i].verteces[curcount] = n / 2 - 1 - i;
			a[i].weights[curcount] = weight;
			a[n / 2 - 1 - i].verteces[curcount] = i;
			a[n / 2 - 1 - i].weights[curcount] = weight;
		}
		
		// инициализация четвертого блока
		for(int i = n / 2; i < 3 * n / 4; i++){
			int weight = (rand () % (MAX_WEIGHT - 1)) + 1;
			a[i].verteces[curcount] = 3 * n / 2 - i - 1;
			a[i].weights[curcount] = weight;
			a[3 * n / 2 - i - 1].verteces[curcount] = i;
			a[3 * n / 2 - i - 1].weights[curcount] = weight;
		}
		curcount = 1;
		
		// инициализцая 2-го и 3-го блоков
		for(int i = 0; i < n / 2; i++){
			int weight = (rand () % (MAX_WEIGHT - 1)) + 1;
			a[i].verteces[curcount] = n - i - 1;
			a[i].weights[curcount] = weight;
			a[n - i - 1].verteces[curcount] = i;
			a[n - i - 1].weights[curcount] = weight;
		}
		
		*ap = a;
		
		
		//Read & send. Каждый процесс получит localNumOfElements строк
		for (i = 1; i < numberOfProcessors; i++) {
			// отправить число вершин
			MPI_Send(&n, 1, MPI_INTEGER, i, SEND_NUM_TAG, MPI_COMM_WORLD);
			// отправить смещения
			MPI_Send(*displs, numberOfProcessors, MPI_INTEGER, i, SEND_DISPLS_TAG, MPI_COMM_WORLD);
			// отправить числа строк для каждого процесса
			MPI_Send(*localNumOfElements, numberOfProcessors, MPI_INTEGER, i, SEND_ELEMNTS_TAG, MPI_COMM_WORLD);
			
			
			for(int j = 0; j < (*localNumOfElements)[i]; j++){
				// отправить кол-во смежных вершин
				int count = a[(*(displs))[i] + j].count;
				MPI_Send(&count, 1, MPI_INTEGER, i, SEND_COUNTS_TAG, MPI_COMM_WORLD);
				MPI_Send(a[(*(displs))[i] + j].weights, count, MPI_INT, i, SEND_WEIGHT_TAG, MPI_COMM_WORLD);
				MPI_Send(a[(*(displs))[i] + j].verteces, count, MPI_INT, i, SEND_VERTECES_TAG, MPI_COMM_WORLD);
			}
		}
		
		fclose(fp);
    } else {// Все узлы, кроме первого, получать стоки матрицы (графа)
		
		
		
		int count;
		// получить число вершин
		MPI_Recv(&n, 1, MPI_INTEGER, MAIN_PROCESS, SEND_NUM_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		*displs = malloc(numberOfProcessors * sizeof(**displs));
		// получить смещение
		MPI_Recv(*displs, numberOfProcessors, MPI_INTEGER, MAIN_PROCESS, SEND_DISPLS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		*localNumOfElements = malloc(numberOfProcessors * sizeof(**localNumOfElements));
		MPI_Recv(*localNumOfElements, numberOfProcessors, MPI_INTEGER, MAIN_PROCESS, SEND_ELEMNTS_TAG, MPI_COMM_WORLD,
				MPI_STATUS_IGNORE);
		
		a = (vertex*)malloc((*localNumOfElements)[rank] * sizeof(vertex));
		

		for(int i = 0; i < (*localNumOfElements)[rank]; i++){
			// получить размер массива весов
			MPI_Recv(&count, 1, MPI_INTEGER, MAIN_PROCESS, SEND_COUNTS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			a[i].count = count;
			a[i].verteces = (int*)malloc(count * sizeof(int));
			a[i].weights = (int*)malloc(count * sizeof(int));
			// получить массив смежных вершин
			MPI_Recv(a[i].verteces, count, MPI_INT, MAIN_PROCESS, SEND_VERTECES_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			// получить массив весов
			MPI_Recv(a[i].weights, count, MPI_INT, MAIN_PROCESS, SEND_WEIGHT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		
		*ap = a;
		
	}
	
	
	
	
	*np = n;
	MPI_Barrier(MPI_COMM_WORLD);
}

// смежна ли вершина
int isNeighbor(int* array, int size, int ver){
	for(int i = 0; i < size; i++){
		if(array[i] == ver){
			return 1;
		}
	}
	return 0;
}

static void dijkstra(
		int const n,
		vertex const *const a,
		int **const result, int rank, int * displs, int * localNumOfElements, int numberOfProcessors
) {
	const int source = 0;
	int sourceNode = 0;
	
	int loc_min[2], glbl_min[2];
	
	char *visited = NULL;
	int *resultVector = NULL;
	int * localResult = NULL;
	
	int start_pos = displs[rank];
	int end_pos = start_pos + localNumOfElements[rank];
	// DEBUGING
	//printf("rank = %d, displs[%d] = %d, localNumOfElements[%d] = %d\n", rank, rank, displs[rank], rank, localNumOfElements[rank]);
	
	//массив для записей уже посещенных вершин
	//0 - непосещенная вершина.
	visited = calloc(n, sizeof(int));
	
	// массив для хранения расстояний от заданной вершины до других
	resultVector = malloc(n * sizeof(*resultVector));
	localResult = malloc(n * sizeof(*resultVector));
	
	
	//localResult[0] = INFINT;
	for(int i = 0; i < n; i++){
		resultVector[i] = INFINT;
		localResult[i] = INFINT;
	}
	
	resultVector[source] = localResult[source] = 0;
	
	do{
	
		loc_min[0] = INFINT;
		loc_min[1] = INFINT;
			
		
		// Найти локальную вершину с минимальным весом и обновить минимальное расстояние 
		// до каждой вершины, с которой она связана.
		for (int i = start_pos; i < end_pos; i++) {
			if (!visited[i] && resultVector[i] < loc_min[0]) {
				loc_min[0] = resultVector[i];
				loc_min[1] = i;
			}
			localResult[i] = resultVector[i];
		}
		MPI_Allreduce(loc_min, glbl_min, 1, MPI_2INT, MPI_MINLOC, MPI_COMM_WORLD);
		printf("Выбрана вершина %d. Минимальный вес: %d\n", glbl_min[1], glbl_min[0]);
		
		
		// каждый процесс сохраняет в свою позицию localResult 
		// значение суммы минимального веса и элемента из массива a.
		if(glbl_min[1] != INFINT){
			
			//Для всех вершин graph[i] таких, что graph[i] смежна с glbl_min[1]
			for (int i = 0; i < localNumOfElements[rank]; i++){
				// если вершина смежна с минимумом
				if(isNeighbor(a[i].verteces, a[i].count, glbl_min[1])){
					for(int j = 0; j < a[i].count; j++){
						if( a[i].weights[j] + glbl_min[0] < localResult[ a[i].verteces[j] ])
							localResult[ a[i].verteces[j] ] = a[i].weights[j] + glbl_min[0];
					}
				}
			}
				
			// сохранение результата в адресном пространстве всех процессов 
			// (localResult в resultVector, при этом в resultVector будут 
			// сохранены минимальные значения из localResult всех процессов)
			MPI_Allreduce(localResult, resultVector, n, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
			//MPI_Barrier(MPI_COMM_WORLD);
			visited[glbl_min[1]] = 1;
		}
	
	} while(glbl_min[1] < INFINT);
	
	free(visited);
	*result = resultVector;
}

static void print_time(double const seconds) {
    printf("Operation Time: %0.04fs\n", seconds);
}

// Записать в файл длины путей от заданной вершины ко всем остальным
static void print_numbers(
		char const *const filename,
		int const n,
		int const *const numbers) {
	int i;
	FILE *fout;
	
	if (NULL == (fout = fopen(filename, "w"))) {
		fprintf(stderr, "error opening '%s'\n", filename);
		abort();
	}
	
	for (i = 0; i < n; i++) {
		fprintf(fout, "%d\n", numbers[i]);
	}
	fclose(fout);
}

int main(int argc, char **argv) {
	int n, numberOfProcessors, rank;
	double t1, t2;
	vertex *a = NULL;
	int *result = NULL;
	int * displs = NULL, *localNumOfElements = NULL;
	
	if (argc < 3) {
		fprintf(stderr, "Usage: %s <graph> <output_file>.\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcessors);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	load(argv[1], &n, &a, numberOfProcessors, &displs, &localNumOfElements, rank);
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	
	t1 = MPI_Wtime();
	
	dijkstra(n, a, &result, rank, displs, localNumOfElements, numberOfProcessors);
	t2 = MPI_Wtime();
	
	if (rank == MAIN_PROCESS) {
		print_time(t2 - t1);
		print_numbers(argv[2], n, result);
		
	}
	
	/*
	// free memory
	for(int i = 0; i < n; i++){
		free(a[i].verteces);
		free(a[i].weights);
		
	}
	free(a);
	free(result);
	*/
	MPI_Finalize();
	return EXIT_SUCCESS;
}

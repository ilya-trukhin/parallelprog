#include <vector>

class Grid
{
public:

	bool FirstBlock; // Если слева есть теневая грань
	bool LastBlock; // Если справа есть теневая грань
	int N; // Размерность матрицы
	int depth; // Глубина матрица
	std::vector<double> gridValues; // Массив элементов сетки
	// Конструктор
	Grid(int n, int d);
	~Grid() {}

	double getValue(int i, int j, int k);
};


#include "grid.h"
#include <vector>

Grid::Grid (int n, int d){
	N = n;
	depth = d;
	gridValues = std::vector<double>(n * n * depth);
}

double Grid::getValue(int i, int j, int k) {
	return gridValues[i*N*N + j * N + k];
}


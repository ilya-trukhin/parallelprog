#include "poisson.h"
#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <math.h>

//функция для начальных значений
double u(double x, double y, double z){
	return x*x+y*y+z*z;
};

// Заполняем сетку начальными значениями
void CreateGrid(Grid &grid, double size_c, double bias, int rank, int size){
	int depth = grid.depth;
	int N = grid.N;
	for (int i = 0; i < depth; i++){
		for (int j = 0; j < N; j++){
			for (int k = 0; k < N; k++){
				if ((grid.FirstBlock && i == 0) || (grid.LastBlock && i == depth - 1) ||
					(j == 0) || (j == N - 1) || (k == 0) || (k == N - 1)){
					grid.gridValues[i*N*N + j * N + k] = u(bias + i * size_c, j*size_c, k*size_c);
				}
			}
		}
	}
}

int main(int argc, char **argv) {

	int size = -1;
	int rank = -1;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	int N = 300; // размер матрицы
	double eps = 1e-8; // eps - для выхода
	double step_size = 2.0/(N-1.0);	// размер шага
	int FirstBlock = rank == 0; // Первый блок
	int LastBlock = rank == size - 1; // Последний блок
	int width = 0;
	
	if (rank != 0)
		width = N / size;
	else
		width = N / size + N % size; // ширина блока
	
	double bias = rank == 0 ? 0 : (rank * width - 1) * step_size; // смещение

	//если блок по середине добавляем две скрытые грани
	if (!FirstBlock && !LastBlock){
		width += 2;
	}

	//если блок скраю сетки, и не единственный добавляем одну скрытую грань
	if (FirstBlock ^ LastBlock){
		width += 1;
	}

	// Создаем сетку и смотриv теневые столбцы
	Grid grid(N, width);

	grid.FirstBlock = FirstBlock;
	grid.LastBlock = LastBlock;
	// Инициализируем сетку
	CreateGrid(grid, step_size, bias, rank, size);
	// Решаем уравнение Пуассона
	Poisson ps(grid, eps, step_size, bias, rank, size);
	//ps.print(u);
	// Засекаем время
	double time;
	time = MPI_Wtime();
	// Количество итераций
	int iter = ps.Calculate(u);
	// Сохраняем данные в глобальной переменной
	double global_time = 0.;
	MPI_Allreduce(&time, &global_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	int global_iter = -1;
	MPI_Allreduce(&iter, &global_iter, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
	if (rank == 0)
		std::cout << "num proc: " << size << "\n" << "N: " << N << "\nstep_size: " << step_size << "\nwidth: " << width << "\n" << "iter: " << global_iter << "\n" << "time: " << MPI_Wtime() - global_time << std::endl;
	// Завершаем процессы
	MPI_Finalize();
	return 0;
}


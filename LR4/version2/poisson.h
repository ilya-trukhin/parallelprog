#include "grid.h"
#include <mpi.h>
#include <iostream>
#include <functional>
#include <algorithm>

#define ALPHA 1e-5

class Poisson
{
private:
	int l_rank;
	int r_rank;
	int rank;
	double step_size;
	double bias;
	double eps;
	Grid nextGrid;
	Grid prevGrid;

public:
	Poisson(Grid &g, double eps, double s_c, double bias, int rank, int size) :
	nextGrid(std::move(g)),
	prevGrid(nextGrid),
	bias(bias),
	eps(eps), step_size(s_c),
	l_rank((rank - 1 + size) % size),
	r_rank((rank + 1) % size),
	rank(rank) {}
	
	~Poisson() {}
	
	double f(double func_u);

	double jakobi(int start, int end, double max_norm, double (*func_u)(double,double,double));

	int Calculate(double (*func_u)(double,double,double));
};


#include "poisson.h"

double Poisson::f(double func_u){
	return 6 - ALPHA * func_u;
};

double Poisson::jakobi(int start, int end, double max_norm, double (*func_u)(double,double,double)){
	double F1, F2, F3;
	int N = prevGrid.N;
	double coef = 1.0 / ((6.0 / step_size * step_size) + ALPHA);

	for (int i = start; i < end; i++){
		for (int j = 1; j < N - 1; j++){
			for (int k = 1; k < N - 1; k++){
				double prev = nextGrid.getValue(i, j, k);
				F1 = prevGrid.gridValues[(i - 1) * N * N + j * N + k] + prevGrid.gridValues[(i + 1) * N * N + j * N + k];
				F2 = prevGrid.gridValues[i * N * N + (j - 1) * N + k] + prevGrid.gridValues[i * N * N + (j + 1) * N + k];
				F3 = prevGrid.gridValues[i * N * N + j * N + k - 1] + prevGrid.gridValues[i * N * N + j * N + k + 1];
				nextGrid.gridValues[i * N * N + j * N + k] = coef * (F1 + F2 + F3 - f(func_u(i * N * N, j * N, k)))/4;
				double norm = abs(prev - nextGrid.getValue(i, j, k));
				if (norm > max_norm) {
					max_norm = norm;
					//std::cout << "Max norm = " << max_norm << std::endl;
				}
			}
		}
	}
	return max_norm;
}

int Poisson::Calculate(double (*func_u)(double,double,double)){
	MPI_Request l_send_req, l_recv_req, r_send_req, r_recv_req;
	double global_max_norm;
	int size = prevGrid.N;
	int width = prevGrid.depth;
	double max_norm = 0.;
	int iter = 0;
	do{
		//std::cout << iter << std::endl;
		max_norm = 0.;
		global_max_norm = 0.;
		prevGrid = nextGrid;
		if (!nextGrid.FirstBlock) {
			max_norm = jakobi(1, 2, max_norm, func_u);
			MPI_Isend(&nextGrid.gridValues[size*size], size*size, MPI_DOUBLE, l_rank, 0, MPI_COMM_WORLD, &l_send_req);
		}
		if (!nextGrid.LastBlock) {
			max_norm = jakobi(width - 2, width - 1, max_norm, func_u);
			MPI_Isend(&nextGrid.gridValues[(width - 2)*size*size], size*size, MPI_DOUBLE, r_rank, 0, MPI_COMM_WORLD, &r_send_req);
		}
		max_norm = jakobi(2, width - 2, max_norm, func_u);
		if (!nextGrid.FirstBlock){
			MPI_Irecv(&nextGrid.gridValues[0], size*size, MPI_DOUBLE, l_rank, 0, MPI_COMM_WORLD, &l_recv_req);
		}
		if (!nextGrid.LastBlock){
			MPI_Irecv(&nextGrid.gridValues[(width - 1)*size*size], size*size, MPI_DOUBLE, r_rank, 0, MPI_COMM_WORLD, &r_recv_req);
		}
		if (!nextGrid.FirstBlock){
			MPI_Wait(&l_send_req, MPI_STATUS_IGNORE);
			MPI_Wait(&l_recv_req, MPI_STATUS_IGNORE);
		}
		if (!nextGrid.LastBlock){
			MPI_Wait(&r_send_req, MPI_STATUS_IGNORE);
			MPI_Wait(&r_recv_req, MPI_STATUS_IGNORE);
		}
		MPI_Allreduce(&max_norm, &global_max_norm, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
		if(rank == 0) 
			std::cout << "global_max_norm = " << global_max_norm << std::endl;
	} while (global_max_norm > eps && iter++ < 5000);

	if(rank == 0) 
		std::cout << "Norm " << global_max_norm << std::endl;
	std::cout << std::endl;

	return iter;
}


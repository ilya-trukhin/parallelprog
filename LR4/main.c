#include <stdio.h>
#include "jacobi.h"

#define ARGS_COUNT 10

int main(int argc, char* argv[]) {
	int rank = 0;
	int size = 1;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	if (argc < ARGS_COUNT) {
		if (rank == 0) {
			fprintf(stderr, "Wrong number of arguments!\n");
			fprintf(stderr, "Enter:\n<D.x> <D.y> <D.z>\n");
			fprintf(stderr, "<N.x> <N.y> <N.z>\n");
			fprintf(stderr, "<p0.x> <p0.y> <p0.z>\n");
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	Point D = (Point){atoi(argv[1]), atoi(argv[2]), atoi(argv[3])};
	Point N = (Point){atoi(argv[4]), atoi(argv[5]), atoi(argv[6])};
	DPoint p0 = (DPoint){atof(argv[7]), atof(argv[8]), atof(argv[9])};

	MPI_Barrier(MPI_COMM_WORLD);

	double t1 = MPI_Wtime();
	PointResult res;
	startSolver(D, N, p0, &res);

	MPI_Barrier(MPI_COMM_WORLD);
	double t2 = MPI_Wtime();

	if (rank == 0) {
		printf("Final difference: %.15lf\n", res.final_diff);
		printf("Iters:  %zu\n", res.iters);

		//printf("Elapsed time: %lf\n", t2 - t1);
	}
	printf("Elapsed time: %lf\n", t2 - t1);
	MPI_Finalize();

	return 0;
}

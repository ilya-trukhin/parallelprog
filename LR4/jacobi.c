#include "jacobi.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

// Номер главного ранка
#define MAIN_PROC_RANK 0

// Минимальный размер сетки по координате D.x
#define DX_SIZE_MIN 3

// Искомая функция
#define phi(x, y, z) ((x) * (x) + (y) * (y) + (z) * (z))

// Правая часть уравнения
#define ro(x, y, z) (6 - ALPHA * phi(x, y, z))

#define max(x, y) ((x) > (y) ? (x) : (y))


// Составление краевых условий 1-го рода
static void inic(double* grid_data, Point D, DPoint h, DPoint p0) {
	for (size_t i = 0; i != D.x; i++){
		for (size_t j = 0; j != D.y; j++){
			for (size_t k = 0; k != D.z; k++) {
				double x = 0;
				double y = 0;
				double z = 0;
				// Проверка краевых элементов
				if (i == 0 || i == D.x - 1 || j == 0 || j == D.y - 1 || k == 0 || k == D.z - 1) {
					x = p0.x + i * h.x;
					y = p0.y + j * h.y;
					z = p0.z + k * h.z;
				}
				grid_data[i * D.y * D.z + j * D.z + k] = phi(x, y, z);
			}
		}
	}
}

// Формула итерационного процесса Якоби
static double jacobi(double* grid_data, Point D, DPoint h, DPoint p0, 
						size_t i, size_t j, size_t k, int offset) {
	double hx2 = h.x * h.x;
	double hy2 = h.y * h.y;
	double hz2 = h.z * h.z;

	double phix = (grid_data[(i-1) * D.y * D.z + j * D.z + k] + 
					grid_data[(i+1) * D.y * D.z + j * D.z + k]) / hx2; 

    double phiy = (grid_data[i * D.y * D.z + (j-1) * D.z + k] + 
					grid_data[i * D.y * D.z + (j+1) * D.z + k]) / hy2;

	double phiz = (grid_data[i * D.y * D.z + j * D.z + (k-1)] + 
					grid_data[i * D.y * D.z + j * D.z + (k+1)]) / hz2;

	double x = p0.x + (i+offset) * h.x;
	double y = p0.y + j * h.y;
	double z = p0.z + k * h.z;

	return (phix + phiy + phiz - ro(x, y, z)) / (2 / hx2 + 2 / hy2 + 2 / hz2 + ALPHA);
}

// оценка точности полученного решенеия
void check_solver(double* data, Point D, DPoint h, DPoint p0){
	// оценка точности полученного результата
	double max_error = 0;
	for(int i = 0; i < D.x; i++){
		for(int j = 0; j < D.y; j++){
			for(int k = 0; k < D.z; k++){
				printf("%lf\n", data[i * D.y * D.z + j * D.z + k]);
				max_error = max(max_error, 
								fabs(data[i * D.y * D.z + j * D.z + k] - 
								phi(p0.x + i*h.x, p0.y + j*h.y, p0.z + k*h.z)));
			}
		}
	}
	printf("max_error = %lf\n", max_error);
}

// Последовательное решение уравнения
static void sequentialSolution(Point D, Point N, DPoint p0, PointResult* result) {
	DPoint h = (DPoint){1.0 * D.x / (N.x - 1), 
						1.0 * D.y / (N.y - 1), 
						1.0 * D.z / (N.z - 1)};

	// Создание сетки
	double* grid_data = (double*)malloc(sizeof(double) * D.x * D.y * D.z);

	// Инициализация сетки
	inic(grid_data, D, h, p0);
	//printf("Boundary has set\n");
	
	// Сетка последующих итераций
	double* new_data = (double*)malloc(sizeof(double) * D.x * D.y * D.z);

	memcpy(new_data, grid_data, sizeof(double) * D.x * D.y * D.z);

	double jacobi_result = 0;
	size_t iters = 0;

	do {
		jacobi_result = 0;

		// Вычисление функции в узлах сетки
		for (size_t i = 1; i != D.x - 1; i++)
			for (size_t j = 1; j != D.y - 1; j++)
				for (size_t k = 1; k != D.z - 1; k++) {
					new_data[i * D.y * D.z + j * D.z + k] = jacobi(grid_data, D, h, p0, i, j, k, 0);

					jacobi_result = max(jacobi_result, fabs(grid_data[i * D.y * D.z + j * D.z + k] - 
															new_data[i * D.y * D.z + j * D.z + k]));
				}
		// поместить в grid_data окнчательный результат
		memcpy(grid_data, new_data, sizeof(double) * D.x * D.y * D.z);

		++iters;
		// Проверка порога сходимости и максимального кол-ва итераций
		// Проверка порога сходимости и максимального кол-ва итераций
	} while (EPS < jacobi_result && iters < ITERS_MAX);

	// оценка точности полученного результата
	check_solver(grid_data, D, h, p0);
	
	free(grid_data);
	free(new_data);

	*result = (PointResult){jacobi_result, iters};
}

// Отправка граничных элементов между процессами
static void sendBorders(int rank, int size, 
						MPI_Request* down_send, MPI_Request* down_recv, 
						MPI_Request* up_send, MPI_Request* up_recv,
						double* data, int local_size, int borders_offset, size_t block_size) {
	// Отправка и приём верхних границ
	if (rank < size - 1) {
		MPI_Isend(data + (local_size + borders_offset - block_size), block_size, 
					MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, down_send);
		MPI_Irecv(data + (local_size + borders_offset), block_size, 
					MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, down_recv);
	}

	// Отправка и приём нижних границ
	if (0 < rank) {
		MPI_Isend(data + borders_offset, block_size, 
					MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, up_send);
		MPI_Irecv(data, block_size, MPI_DOUBLE, rank - 1, 0, 
					MPI_COMM_WORLD, up_recv);
	}
}

// Приём граничных элементов
static void recvBorders(int rank, int size,
						MPI_Request* down_send, MPI_Request* down_recv, 
						MPI_Request* up_send, MPI_Request* up_recv) {
	// Ожидание верхних границ
	if (rank < size - 1) {
		MPI_Wait(down_send, MPI_STATUSES_IGNORE);
		MPI_Wait(down_recv, MPI_STATUSES_IGNORE);
	}

	// Ожидание нижних границ
	if (0 < rank) {
		MPI_Wait(up_send, MPI_STATUSES_IGNORE);
		MPI_Wait(up_recv, MPI_STATUSES_IGNORE);
	}
}

// Обработка блока данных по оси D.x
static double calcBlock(double* grid_data, double* new_data, Point D, DPoint h, DPoint p0,
						size_t x_beg, size_t x_end, double current_result, int offset) {
	double result = current_result;

	// Вычисление функции в узлах блока
	for (size_t i = x_beg; i != x_end; i++)
		for (size_t j = 1; j != D.y - 1; j++)
			for (size_t k = 1; k != D.z - 1; k++) {
				new_data[i * D.y * D.z + j * D.z + k] = jacobi(grid_data, D, h, p0, i, j, k, offset);
				result = max(result, fabs(grid_data[i * D.y * D.z + j * D.z + k] - 
											new_data[i * D.y * D.z + j * D.z + k]));
			}
	return result;
}

// Обработка поля данных (блоков, не доходящих до границ)
static double calcField(int rank, int size, double* data, double* new_data, 
							int local_size, int borders, size_t block_size,
							Point D, DPoint h, DPoint p0, double current_result, int offset) {
	double result = current_result;

	// Обработка блока 0-го процесса
	if (rank == 0) {
		size_t x_beg = 1;
		size_t x_end = (local_size + borders) / block_size - 2;
		result = calcBlock(data, new_data, D, h, p0, x_beg, x_end, result, offset);
	}

	// Обработка блока последнего процесса
	if (rank == size - 1) {
		size_t x_beg = 2;
		size_t x_end = (local_size + borders) / block_size - 1;
		result = calcBlock(data, new_data, D, h, p0, x_beg, x_end, result, offset);
	}

	// Обработка центральных блоков
	if (0 < rank && rank < size - 1) {
		size_t x_beg = 2;
		size_t x_end = (local_size + borders) / block_size - 2;
		result = calcBlock(data, new_data, D, h, p0, x_beg, x_end, result, offset);
	}

	return result;
}

// Обработка границ
static double calcBorders(int rank, int size, double* data, double* new_data,
							int local_size, int borders, size_t block_size,
							Point D, DPoint h, DPoint p0, double current_result, int offset) {
	double result = current_result;

	size_t x_bottom = (local_size + borders) / block_size - 2;
	size_t x_upper = 1;

	// Обработка нижней границы
	if (rank < size - 1)
		result = calcBlock(data, new_data, D, h, p0, x_bottom, x_bottom + 1, result, offset);
    
	// Обработка верхней границы
	if (0 < rank)
		result = calcBlock(data, new_data, D, h, p0, x_upper, x_upper + 1, result, offset);

	return result;
}

// Параллельное решение уравнения
static void parallelSolution(Point D, Point N, DPoint p0, int rank, int size, PointResult* result) {
	PointResult root_result;

	double* grid_data = NULL;

	size_t block_size = D.y * D.z;
	DPoint h = (DPoint){1.0 * D.x / (N.x - 1), 
						1.0 * D.y / (N.y - 1), 
						1.0 * D.z / (N.z - 1)};

	// Инициализация поля данных (сетки)
	if (rank == MAIN_PROC_RANK) {
		grid_data = (double*)malloc(sizeof(double) * D.x * D.y * D.z);

		inic(grid_data, D, h, p0);
	}

	// Массив размеров локальных данных
	int* local_sizes = (int*)malloc(sizeof(int) * size);
    
	// Массив размеров сдвигов локальных данных
	int* offsets = (int*)malloc(sizeof(int) * size);

	int chunk_size = D.x / size;
	int remainder = D.x % size;

	// Определение размеров локальных данных
	int shift = 0;
	for (int i = 0; i != size; i++) {
		local_sizes[i] = chunk_size + (i < remainder ? 1 : 0);

		offsets[i] = shift;							// Сдвиг строки локальных данных
		shift += local_sizes[i];

		local_sizes[i] *= block_size;				// Получение размера локального блока
		offsets[i] *= block_size;					// Получение сдвига блока данных
	}

	// Определение границ и сдвигов границ
	int borders = 2 * block_size;					// Кол-во границ
	int borders_offset = block_size;				// Сдвиг границ

	if (rank == 0 || rank == size - 1) {
		borders = block_size;
		if (rank == 0)
			borders_offset = 0;
	}

	// Выделение памяти под блок данных и границы
	double* local_data = (double*)malloc(sizeof(double) * (local_sizes[rank] + borders));

	// Разбиение поля данных на блоки разного размера и передача другим процессам
	MPI_Scatterv(grid_data, local_sizes, offsets, MPI_DOUBLE, local_data + borders_offset, 
				local_sizes[rank], MPI_DOUBLE, MAIN_PROC_RANK, MPI_COMM_WORLD);

	// Сетка последующих итераций
	double* new_data = (double*)malloc(sizeof(double) * (local_sizes[rank] + borders));

	memcpy(new_data, local_data, sizeof(double) * (local_sizes[rank] + borders));

	double jacobi_result = 0;
	size_t iters = 0;
	do {
		jacobi_result = 0;

		MPI_Request down_send;
		MPI_Request down_recv;
		MPI_Request up_send;
		MPI_Request up_recv;

		// Посылаем границы другим процессам
		sendBorders(rank, size, &down_send, &down_recv, &up_send, &up_recv, 
					local_data, local_sizes[rank], borders_offset, block_size);

		// Обрабатываем поле данных (без границ)
		jacobi_result = calcField(rank, size, local_data, new_data, 
									local_sizes[rank], borders, block_size, 
									D, h, p0, jacobi_result, offsets[rank]);

		// Принимаем границы
		recvBorders(rank, size, &down_send, &down_recv, &up_send, &up_recv);

		// Обрабатываем границы
		calcBorders(rank, size, local_data, new_data, 
					local_sizes[rank], borders, block_size, 
					D, h, p0, jacobi_result, offsets[rank]);

		// Обмениваемся результатми между процессами и находи max
		MPI_Allreduce(MPI_IN_PLACE, &jacobi_result, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

		// Обновление сетки для следующей итерации
		double* tmp = local_data;
		local_data = new_data;
		new_data = tmp;
        
		++iters;
		// Проверка порога сходимости и максимального кол-ва итераций
	} while (EPS < jacobi_result && iters < ITERS_MAX);
	
	

	if (rank == MAIN_PROC_RANK) {
		printf("Checking result ...\n");
		//check_solver(grid_data, D, h, p0);
		check_solver(new_data, D, h, p0);
		free(grid_data);

		*result = (PointResult){jacobi_result, iters};
	}
	free(new_data);
	free(local_sizes);
	free(offsets);
}

void startSolver(Point D, Point N, DPoint p0, PointResult* result) {
	//int rank = MAIN_PROC_RANK;
	//int size = 1;
	int rank, size;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (size == 1 || D.x / size < DX_SIZE_MIN)
		sequentialSolution(D, N, p0, result);
    
	else {
		parallelSolution(D, N, p0, rank, size, result);
	}
}

#ifndef JACOBI3D_H
#define JACOBI3D_H

#include <stdlib.h>
#include <mpi.h>

#define ALPHA 10E+5         // Параметр уравнения
#define EPS   10E-8         // Порог сходимости

#define ITERS_MAX 10000      // Кол-во итераций метода

typedef struct Point {
    size_t x;
    size_t y;
    size_t z;
} Point;

typedef struct DPoint {
    double x;
    double y;
    double z;
} DPoint;

typedef struct PointResult {
    double final_diff;
    size_t iters;
} PointResult;

void startSolver(Point D, Point N, DPoint p0, PointResult* result);

#endif

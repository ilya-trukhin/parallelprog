#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>
#include <sys/time.h>
#include <malloc.h>

#include <list>

#define LOOPS 10000000
#define MAX_THREAD_COUNT 10

using namespace std;

list<int> the_list;

#ifdef USE_SPINLOCK
pthread_spinlock_t spinlock;
#else
pthread_mutex_t mutex;
#endif

pid_t gettid() { return syscall( __NR_gettid ); }

void *consumer(void *ptr)
{

    //printf("Consumer TID %lu\n", (unsigned long)gettid());

    while (1)
    {
#ifdef USE_SPINLOCK
        pthread_spin_lock(&spinlock);
#else
        pthread_mutex_lock(&mutex);
#endif

        if (the_list.empty())
        {
#ifdef USE_SPINLOCK
            pthread_spin_unlock(&spinlock);
#else
            pthread_mutex_unlock(&mutex);
#endif
            break;
        }

        the_list.front();
        the_list.pop_front();

#ifdef USE_SPINLOCK
        pthread_spin_unlock(&spinlock);
#else
        pthread_mutex_unlock(&mutex);
#endif
    }

    return NULL;
}

int main()
{
    
    struct timeval tv1, tv2;

#ifdef USE_SPINLOCK
    pthread_spin_init(&spinlock, 0);
#else
    pthread_mutex_init(&mutex, NULL);
#endif

    // Creating the list content...
    for (int i = 0; i < LOOPS; i++)
        the_list.push_back(i);

	for(int i = 2; i <= MAX_THREAD_COUNT; i++){
		pthread_t *thr = (pthread_t*)malloc(sizeof(pthread_t) * i);
		// Measuring time before starting the threads...
		gettimeofday(&tv1, NULL);

		for(int j = 0; j < i; j++){
			pthread_create(&thr[j], NULL, consumer, NULL);
		//pthread_create(&thr2, NULL, consumer, NULL);
		}
		
		for(int j = 0; j < i; j++){
			pthread_join(thr[j], NULL);
		}
		//pthread_join(thr2, NULL);

		// Measuring time after threads finished...
		gettimeofday(&tv2, NULL);

		if (tv1.tv_usec > tv2.tv_usec)
		{
			tv2.tv_sec--;
			tv2.tv_usec += 1000000;
		}
		printf("Thread count - %d\n", i);
		printf("Result - %ld.%ld\n", tv2.tv_sec - tv1.tv_sec, tv2.tv_usec - tv1.tv_usec);
		
		free(thr);
    }

#ifdef USE_SPINLOCK
    pthread_spin_destroy(&spinlock);
#else
    pthread_mutex_destroy(&mutex);
#endif

    return 0;
}

#include "mapreduce.h"
#include <pthread.h>
#include <malloc.h>

pthread_mutex_t mutex;

typedef struct mapThreadParam{
	std::multimap <int, int> (*mapFcn)(int);	// pointer to map function
	size_t start_index;					// index of first item in array for thread
	size_t end_index;					// index of last item in array for thread
	std::vector<int> mapSource;
	std::multimap <int, int> *mapResults;
} mapThreadParam_t;

typedef struct reduceThreadParam{
	std::pair<int, int> (*reduceFcn)(std::map<int, std::vector<int>>::iterator);	// pointer to reduce function
	std::map<int, std::vector<int>>::iterator start_iter;					// index of first item in array for thread
	size_t item_count;					// count of elements for thread
	std::map<int, std::vector<int>> reduceSource;
	std::vector<std::pair<int, int>> *reduceResults;
} reduceThreadParam_t;


void* mapFunc(void* arg){
	mapThreadParam_t* param = (mapThreadParam_t*)arg;
	std::multimap <int, int> (*mapFcn)(int) = param->mapFcn;
	size_t start_index = param->start_index;
	size_t end_index = param->end_index;
	std::vector<int> mapsource = param->mapSource;
	std::multimap <int, int> *mapResults = param->mapResults;
	free(param);
	
	/*
	printf("start index = %lu\n", start_index);
	printf("end index = %lu\n", end_index);
	for(int i = start_index; i < end_index; i++){
		printf("%lu - %d\n", start_index, mapsource[i]);
	}
	*/
	
	for(int i = start_index; i < end_index; i++){
		std::multimap <int, int> tmp = mapFcn(mapsource[i]);
		pthread_mutex_lock(&mutex);
		for(std::multimap<int, int>::iterator it = tmp.begin(); it != tmp.end(); it++){
			mapResults->insert(std::pair<int, int>(it->first, it->second));
		}
		pthread_mutex_unlock(&mutex);
	}
	
	return NULL;
}

void* reduceFunc(void* arg){
	reduceThreadParam_t* param = (reduceThreadParam_t*) arg;
	std::pair<int, int> (*reduceFcn)(std::map<int, std::vector<int>>::iterator) = param->reduceFcn;	
	std::map<int, std::vector<int>>::iterator start_iter = param->start_iter;					
	size_t item_count = param->item_count;
	std::map<int, std::vector<int>> reduceSource = param->reduceSource;
	std::vector<std::pair<int, int>> *reduceResults = param->reduceResults;
	free(param);
	
	int count = 0;
	
	for(std::map<int, std::vector<int>>::iterator it = start_iter; count < item_count && it != reduceSource.end(); it++, count++){
		std::pair<int, int> newpair = reduceFcn(it);
		//std::pair<int, int> newpair = reduceFcn(start_iter);
		pthread_mutex_lock(&mutex);
		reduceResults->push_back(newpair);
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}


// MapReduce function
std::vector<std::pair<int, int>> MapReduce( std::multimap <int, int> (*mapFcn)(int),
											std::pair<int, int> (*reduceFcn)(std::map<int, std::vector<int>>::iterator),
											std::vector<int> source, 
											int thread_count
){
	int th_count = thread_count > source.size() ? source.size() : thread_count;
	
	std::multimap <int, int> mapResults;
	std::map<int, std::vector<int>> reduceSource;
	
	std::multimap<int, int>::iterator mapResIter;
	std::map<int, std::vector<int>>::iterator reduceSrcIter;
	
	std::vector<std::pair<int, int>> reduceResults;
	
	// MAP
	pthread_t *mapthread = new pthread_t[th_count];
	
	for(int i = 0; i < th_count; i++){
		mapThreadParam_t *p = (mapThreadParam_t*)malloc(sizeof(mapThreadParam_t));
		p->mapFcn = mapFcn;	// pointer to map function
		p->start_index = i;					// index of first item in array for thread
		p->end_index = i + 1;					// index of last item in array for thread
		p->mapSource = source;
		p->mapResults = &mapResults;
		pthread_create(&mapthread[i], NULL, mapFunc, p);
	}
	
	for(int i = 0; i < th_count; i++){	
		pthread_join(mapthread[i], NULL);
	}
	
	// GROUP ITEMS BY KEY
	for(mapResIter = mapResults.begin(); mapResIter != mapResults.end();  mapResIter++){
		reduceSource[mapResIter->first].push_back(mapResIter->second);
	}
	
	
	// SHOW REDUCE SOURCE
	for(reduceSrcIter = reduceSource.begin(); reduceSrcIter != reduceSource.end(); reduceSrcIter++){
		std::cout << reduceSrcIter->first << ": ";
		for(int i = 0; i < reduceSrcIter->second.size(); i++){
			std::cout << reduceSrcIter->second[i] << " ";
		}
		std::cout << std::endl;
	}
	
	// REDUCE
	pthread_t *reducethread = new pthread_t[reduceSource.size()];
	
	std::cout << "REDUCE RESULT" << std::endl;
	int k = 0;
	for(std::map<int, std::vector<int>>::iterator it = reduceSource.begin(); it != reduceSource.end(); it++, k++){
		//res.push_back(reduceFcn(it));
		reduceThreadParam_t *p = (reduceThreadParam_t*)malloc(sizeof(reduceThreadParam_t));
		p->reduceFcn = reduceFcn;	// pointer to map function
		p->start_iter = it;					// index of first item in array for thread
		p->item_count = 1;					// index of last item in array for thread
		p->reduceSource = reduceSource;
		p->reduceResults = &reduceResults;
		pthread_create(&reducethread[k], NULL, reduceFunc, p);
	}
	
	for(int i = 0; i < reduceSource.size(); i++){	
		pthread_join(reducethread[i], NULL);
	}

	delete[] reducethread;
	delete[] mapthread;
	return reduceResults;
}


int main(int argc, char *argv[]){
		
	pthread_mutex_init(&mutex, NULL);
	std::vector<int> source;
	source.push_back(123);
	source.push_back(24);
	source.push_back(1425);
	source.push_back(42);
	source.push_back(35);
	
	std::vector<std::pair<int, int>> res = MapReduce(Map, Reduce, source, 5);
	
	for(int i = 0; i < res.size(); i++){
		std::cout << res[i].first << ": " << res[i].second << std::endl;
	}
	
	pthread_mutex_destroy(&mutex);
	return 0;
}

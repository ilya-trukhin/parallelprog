#include "factorization.h"
#include <map>
#include <iostream>


// Map function
std::multimap <int, int> Map (int value);

// Reduce function
std::pair<int, int> Reduce(std::map<int, std::vector<int>>::iterator reduceSrcIter);


#include "factorization.h"
 
std::vector<int> prime_factorization( int n, bool repeat){
	std::vector<int> factors;
	for ( int d = 2; d <= n; ++d ){
		int k = 0;
		while ( !( n % d ) ){
			n /= d;
			++k;
		}
		if ( repeat ){
			while ( k-- )
				factors.push_back(d);
		}
		else{
			if ( k )
				factors.push_back(d);
		}
	}
	return factors;
}

std::vector<int> prime_factorization( int n ){
	bool k;
	std::vector<int> factors;
	for ( int d = 2; d <= n; ++d ){
		k = false;
		if(!( n % d )){
			k = true;
			while ( !( n % d ) ){
				n /= d;
			}
		}
		
		if ( k )
			factors.push_back(d);
	}
	return factors;
}
 

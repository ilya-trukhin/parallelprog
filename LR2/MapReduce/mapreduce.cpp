#include "mapreduce.h"

// Map function
std::multimap <int, int> Map (int value)
{
	std::multimap <int, int> mapResult;
	std::vector <int> res = prime_factorization( value );
	for(int elem : res){
		mapResult.insert(std::pair <int, int> (elem, value));
	}
	return mapResult;
}

// Reduce function
std::pair<int, int> Reduce(std::map<int, std::vector<int>>::iterator reduceSrcIter){
	//std::pair<int, int> res;
	int sum = 0;
	int key = reduceSrcIter->first;
	
	for(int i = 0; i < reduceSrcIter->second.size(); i++){
		sum += reduceSrcIter->second[i];
	}
	return std::make_pair(key, sum);
}



#include "thpool/thpool.h"
#include "fs.h"
#include <pthread.h>
#include <malloc.h>
#include <curl/curl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <regex.h>

#include <sys/time.h>


#define DEFAULT_URL "http://www.chess.com"
#define DEFUALT_THREAD_COUNT 4
#define MAX_LINKS 500	// maximum number of links that can be visited
#define MAX_URL_LENGTH 1024

#define BUFSIZE 1000000
 
char visited[MAX_LINKS][MAX_URL_LENGTH];

static size_t cur_links_visited_count = 0;

pthread_mutex_t mutex;

typedef struct
{
	char buffer[BUFSIZE];
	size_t buffSize;
} WriteFuncCtx;

typedef struct parser_param{
	char url[MAX_URL_LENGTH];
	threadpool* thpool;
} parser_param;

int find(char storage[MAX_LINKS][MAX_URL_LENGTH], size_t storage_size, const char* str){
	for(size_t i = 0; i < storage_size; i++){
		if(!strcmp(storage[i], str))
			return 1;
	}
	return 0;
}

char* get_end_of_url(char* start_pos, const size_t max_url_length)
{
	for(size_t i = 0; i < max_url_length; i++){
		if(start_pos[i] == '\"' || 
		start_pos[i] == ' ' || 
		start_pos[i] == ',' || 
		start_pos[i] == '%' || 
		start_pos[i] == ';' || 
		start_pos[i] == '{' || 
		start_pos[i] == '}' || 
		start_pos[i] == '(' || 
		start_pos[i] == ')')
			return start_pos + i;
	}
	return NULL;
}

size_t filterit(void *ptr, size_t size, size_t nmemb, void *userdata){
	WriteFuncCtx* ctx = (WriteFuncCtx*)userdata;
	if ( (ctx->buffSize + size*nmemb) > BUFSIZE ) return BUFSIZE;
	memcpy(ctx->buffer + ctx->buffSize, ptr, size*nmemb);
	ctx->buffSize += size*nmemb;
	return size*nmemb;
}

void parse_pages(void* arg){
	//printf("Psrser start!\n");
	parser_param* param = (parser_param*)arg;
	char url[MAX_URL_LENGTH];
	strcpy(url, param->url);
	threadpool* thpool = param->thpool;
	free(param);
	
	CURL *curlHandle;
	regmatch_t amatch;
	regex_t cregex;
	int success;
	char newurl[MAX_URL_LENGTH];
	char* start_pos;
	char* end_pos;
	
	curlHandle = curl_easy_init();
	curl_easy_setopt(curlHandle, CURLOPT_URL, url);
	curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, filterit);
	WriteFuncCtx ctx;
	ctx.buffSize = 0;
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &ctx);
	success = curl_easy_perform(curlHandle);
	curl_easy_cleanup(curlHandle);

	ctx.buffer[ctx.buffSize] = 0;

	regcomp(&cregex, " UTC", REG_NEWLINE);
	regexec(&cregex, ctx.buffer, 1, &amatch, 0);
	int bi = amatch.rm_so;
	while ( bi-- > 0 )
		if ( memcmp(&(ctx.buffer[bi]), "<BR>", 4) == 0 ||
			memcpy(&(ctx.buffer[bi]), "<br>", 4) == 0) 
			break;

	ctx.buffer[amatch.rm_eo] = 0;

	//printf("%s\n", &buffer[bi+2]);	
		
	//write_page(url, ctx.buffer + 2);

	
	memset(newurl, 0, MAX_URL_LENGTH);
  	
  	start_pos = ctx.buffer + 2;
	end_pos = start_pos;

	while(1){
		start_pos = strstr(end_pos, "href=");
		if(!start_pos){
			break;
		}
		//end_pos = strstr(start_pos + 6, "\"");
		end_pos = get_end_of_url(start_pos + 6, MAX_URL_LENGTH);
		if(!end_pos){
			break;
		}
		strncpy(newurl, start_pos + 6, end_pos - start_pos - 6);
  	
		if(strstr(newurl, "http://") || strstr(newurl, "https://")){
			pthread_mutex_lock(&mutex);
			if(!find(visited, cur_links_visited_count, newurl) && cur_links_visited_count < MAX_LINKS){
				strcpy(visited[cur_links_visited_count++], newurl);
				//puts(newurl);
				parser_param* newparam = (parser_param*)malloc(sizeof(parser_param));
				strcpy(newparam->url, newurl);
				newparam->thpool = thpool;
				thpool_add_work(*thpool, &parse_pages, (void*)newparam);
			}
			pthread_mutex_unlock(&mutex);
		}
	}
	
	regfree(&cregex);
}


int main(int argc, char *argv[]){
	char url[100];
	struct timeval tv1, tv2;
	
	int thread_count;
	
	if(argc < 2){
		strcpy(url, DEFAULT_URL);
		printf("Default url is used: %s\n", DEFAULT_URL);
	}
	else{
		strcpy(url, argv[1]);
	}
	
	if(argc == 3){
		thread_count = atoi(argv[2]);
	}
	else{
		thread_count = DEFUALT_THREAD_COUNT;
	}
	
	if(!is_dir_exist(PAGES_DIR)){
		if(create_dir(PAGES_DIR) == EXIT_FAILURE){
			exit(EXIT_FAILURE);
		}
	}
	
	pthread_mutex_init(&mutex, NULL);
	//puts("Making threadpool with 4 threads");
	threadpool thpool = thpool_init(thread_count);

	//int i = 1;
	//for (i=0; i<5; i++){
		//int *arg = (int*)malloc(sizeof(int));
		//*arg = i;
		//thpool_add_work(thpool, &hello, (void*)arg);
		//thpool_add_work(thpool, &task2, NULL);
	//};
	parser_param* param = (parser_param*)malloc(sizeof(parser_param));
	strcpy(param->url, url);
	param->thpool = &thpool;
	
	gettimeofday(&tv1, NULL);
	thpool_add_work(thpool, &parse_pages, (void*)param);
	thpool_wait(thpool);
	
	gettimeofday(&tv2, NULL);

	if (tv1.tv_usec > tv2.tv_usec)
	{
		tv2.tv_sec--;
		tv2.tv_usec += 1000000;
	}
	
	printf("Time spent - %ld.%ld\n", tv2.tv_sec - tv1.tv_sec, tv2.tv_usec - tv1.tv_usec);
	printf("Links visited: %lu\n", cur_links_visited_count);
	//puts("Killing threadpool");
	thpool_destroy(thpool);
	
	pthread_mutex_destroy(&mutex);
	
	return 0;
}

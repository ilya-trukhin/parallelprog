#include "fs.h"

void replacechar(char* str, size_t size, const char oldchar, const char newchar){
	for(size_t i = 0; i < size; i++){
		if(str[i] == oldchar){
			str[i] = newchar;
		}
	}
}
 
int is_dir_exist(const char* pathToDir){
	struct stat sb;
	if (stat(pathToDir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
		// is exist
		return 1;
	} else {
		// isn't exist
		return 0;
	}
}
 
int create_dir(const char* pathToDir){
	if (mkdir(pathToDir, 0770) == -1) { // Создать каталог
		fprintf(stderr, "Error: %s\n", strerror(errno));
		return(EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}


void write_page(const char* page_url, char* buffer){
	char filename[FNAMELEN];
	
	strcpy(filename, PAGES_DIR);
	strcat(filename, page_url);
	replacechar(filename + strlen(PAGES_DIR), strlen(filename), '/', '-');
	
	//printf("filename is: %s\n", filename);
	
	FILE *f = fopen(filename, "w");
	if (f == NULL)
	{
		fprintf(stderr, "Error opening file %s!\n", filename);
		return;
	}
	fprintf(f, "%s", buffer);
	fclose(f);
}

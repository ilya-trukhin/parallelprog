#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>

#define FNAMELEN 150
#define PAGES_DIR "./pages/"

void replacechar(char* str, size_t size, const char oldchar, const char newchar);

int is_dir_exist(const char* pathToDir); 

int create_dir(const char* pathToDir);

void write_page(const char* page_url, char* buffer);
